git clone https://git.savannah.gnu.org/git/emacs.git
cd emacs
git checkout emacs-29.3
sudo yum install ncurses-devel
sudo yum install gnutls-devel
./configure --without-selinux --with-x-toolkit=no --with-xpm=no --with-jpeg=no --with-png=no --with-gif=no --with-tiff=no
make
sudo make install
