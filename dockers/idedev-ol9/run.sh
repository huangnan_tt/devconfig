args=(
#   --rm
    --restart=unless-stopped
    -td
    --name ideol9
    -h ideol9
    --ulimit nofile=262144:262144
    --user $(id -u):inferno \
#    -e DISPLAY=$DISPLAY \
    -w /proj/work
#    -v /tmp/.X11-unix:/tmp/.X11-unix \
    -v $(readlink -e /usr/local):/usr/local \
    -e GOROOT=/usr/local/inferno/copyleft/go1.18
    -v $HOME/.ssh:/home/$USER/.ssh
    -v $(readlink -e /proj/inferno):/proj/inferno
    -v $(readlink -e /proj/work):/proj/work
    -v $(readlink -e /opt/systems):/opt/systems
    -v /proj/inferno/oracle/linux/client64/network/admin:/home/$USER/Oracle/network/admin
    idedevol9
    bash
)

docker run "${args[@]}"
