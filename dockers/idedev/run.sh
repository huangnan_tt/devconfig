args=(
#   --rm
    --restart=unless-stopped
    -td
    --name idedev
    --ulimit nofile=262144:262144
    --expose 3000
    -p 3000:3000
    -h idedev
    --user $(id -u):inferno
#   -e DISPLAY=$DISPLAY
    -w /proj/work
#   -v /tmp/.X11-unix:/tmp/.X11-unix
    -v $(readlink -e /usr/local/inferno/copyleft):/usr/local/inferno/copyleft
    -e GOROOT=/usr/local/inferno/copyleft/go1.18
    -v $HOME/.ssh:/home/$USER/.ssh
    -v $(readlink -e /proj/inferno):/proj/inferno
    -v $(readlink -e /proj/work):/proj/work
    idedev
    bash
)

docker run "${args[@]}"

