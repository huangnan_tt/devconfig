#!/bin/bash

export PROJ_WORK=$(readlink -e $HOME)
export PROJ_INFERNO=$(readlink -e /proj/inferno)
export USR_LOCAL=$(readlink -e /usr/local)
export SCRATCH_LOCALHOST=$(readlink -e /scratch/localhost)
export WORKDIR=${WORKDIR:-repo1}

docker compose "$@"
