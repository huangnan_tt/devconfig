docker run --rm -td --name ol7oci -h ol7oci \
           --user $(id -u):inferno \
           --ulimit nofile=262144:262144 \
           -v $(readlink -e /proj/work):/proj/work \
           -v $HOME/.ssh:$HOME/.ssh \
           -v $(readlink -e /scratch/localhost):/scratch/localhost \
           -w /proj/work/$USER/gitrepos \
           --env TNS_ADMIN=/proj/work/huangnan/Wallet_DB202109261625 \
           --privileged \
           --log-driver json-file --log-opt max-size=10m ol7oci bash
