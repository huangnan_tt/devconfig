#!/bin/bash

export PROJ_WORK=$(readlink -e /proj/work)
export PROJ_INFERNO=$(readlink -e /proj/inferno)
export USR_LOCAL=$(readlink -e /usr/local)
export SCRATCH_LOCALHOST=$(readlink -e /scratch/localhost)
export WORKDIR=${WORKDIR:-repo1}

/opt/systems/bin/docker-compose "$@"
