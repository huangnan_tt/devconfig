docker run -d -it --name ol7build -h ol7build \
           --user $(id -u):inferno --link infbuilddb_0:db \
           --network=infbuild \
           -v $(readlink -e /proj/work):/proj/work \
           -v $HOME:/home/$USER \
           -v $(readlink -e /proj/inferno):/proj/inferno \
           -v $(readlink -e /usr/local/inferno):/usr/local/inferno \
           -v $(readlink -e /scratch/localhost):/scratch/localhost \
           -w /proj/work/$USER/gitrepos/repo1 \
           --log-driver json-file --log-opt max-size=10m ol7build bash
