#!/bin/bash

export PROJ_WORK=$(readlink -e /proj/work)
export PROJ_INFERNO=$(readlink -e /proj/inferno)
export USR_LOCAL_INFERNO=$(readlink -e /usr/local/inferno)
export SCRATCH_LOCALHOST=$(readlink -e /scratch/localhost)

/opt/systems/bin/docker-compose "$@"
