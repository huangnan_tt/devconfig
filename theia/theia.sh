#!/bin/sh
YARN_VERSION=1.22.5
THEIA_DIR=/proj/work/theia
export GOROOT=/usr/local/inferno/copyleft/go1.15.2
export GOPATH=$THEIA_DIR/go-packages
PYTHON_ROOT=/usr/local/inferno/copyleft/python-2.7.17
NODEJS_ROOT=/usr/local/inferno/copyleft/node-v12.4.0-linux-x64
RUST_ROOT=/proj/work/huangnan/copyleft/rust-1.48.0
export PATH=$NODEJS_ROOT/bin:$THEIA_DIR/yarn/bin:$PYTHON_ROOT/bin:$GOPATH/bin:$GOROOT/bin:$RUST_ROOT/bin:/usr/local/inferno/copyleft/cmake-3.16.0/bin:/usr/local/inferno/copyleft/gdb-8.3/bin:$PATH
if [ ! -d "$THEIA_DIR" ]; then
    set -xe
    #
    # Setup theia root directory layout
    #
    #cp package.json $THEIA_DIR
    mkdir $THEIA_DIR
    cd $THEIA_DIR
    mkdir -p node node-src yarn

    #
    # Save current environment
    #

    #
    # Setup nodejs
    #
    export CC=/usr/bin/gcc
    export CXX=/usr/bin/g++

    #
    # Setup YARN (install into yarn subfolder)
    #
    curl -fSLO --compressed "https://yarnpkg.com/downloads/$YARN_VERSION/yarn-v$YARN_VERSION.tar.gz"
    mkdir -p yarn
    tar -xzf yarn-v$YARN_VERSION.tar.gz -C yarn --strip-components=1
    rm yarn-v$YARN_VERSION.tar.gz

    #
    # Setup Theia application
    #
    curl -SLO "https://raw.githubusercontent.com/theia-ide/theia-apps/master/theia-full-docker/latest.package.json"
    mv latest.package.json package.json
    yarn add vscode-jsonrpc@4.0.0
    yarn --pure-lockfile
    NODE_OPTIONS="--max_old_space_size=4096" yarn theia build
    yarn theia download:plugins
    yarn --production
    yarn autoclean --init
    echo *.ts >> .yarnclean
    echo *.ts.map >> .yarnclean
    echo *.spec.* >> .yarnclean
    yarn autoclean --force
    yarn cache clean

    #
    # Setup C++ toolkit
    #

    #
    # Setup golang toolkit
    #
    # Install VS Code Go tools: https://github.com/Microsoft/vscode-go/blob/058eccf17f1b0eebd607581591828531d768b98e/src/goInstallTools.ts#L19-L45
    go get -u -v github.com/mdempsky/gocode && \
    go get -u -v github.com/uudashr/gopkgs/cmd/gopkgs && \
    go get -u -v github.com/ramya-rao-a/go-outline && \
    go get -u -v github.com/acroca/go-symbols && \
    go get -u -v golang.org/x/tools/cmd/guru && \
    go get -u -v golang.org/x/tools/cmd/gorename && \
    go get -u -v github.com/fatih/gomodifytags && \
    go get -u -v github.com/haya14busa/goplay/cmd/goplay && \
    go get -u -v github.com/josharian/impl && \
    go get -u -v github.com/tylerb/gotype-live && \
    go get -u -v github.com/rogpeppe/godef && \
    go get -u -v github.com/zmb3/gogetdoc && \
    go get -u -v golang.org/x/tools/cmd/goimports && \
    go get -u -v github.com/sqs/goreturns && \
    go get -u -v winterdrache.de/goformat/goformat && \
    go get -u -v golang.org/x/lint/golint && \
    go get -u -v github.com/cweill/gotests/... && \
    go get -u -v github.com/alecthomas/gometalinter && \
    go get -u -v honnef.co/go/tools/... && \
    GO111MODULE=on go get github.com/golangci/golangci-lint/cmd/golangci-lint && \
    go get -u -v github.com/mgechev/revive && \
    go get -u -v github.com/sourcegraph/go-langserver && \
    go get -u -v github.com/go-delve/delve/cmd/dlv && \
    go get -u -v github.com/davidrjenni/reftools/cmd/fillstruct && \
    go get -u -v github.com/godoctor/godoctor
    go get -u -v -d github.com/stamblerre/gocode && \
    go build -o $GOPATH/bin/gocode-gomod github.com/stamblerre/gocode
    
else
    cd $THEIA_DIR
fi

export THEIA_DEFAULT_PLUGINS=local-dir:$THEIA_DIR/plugins
node src-gen/backend/main.js --hostname=0.0.0.0 "$@"
#yarn theia start --hostname=0.0.0.0 "$@"
