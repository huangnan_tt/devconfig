#include "MmapCsvFileReader.h"
#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>
#include <cmath>
#include <cstring>
#include <filesystem>
#include <stdexcept>
#include <set>

#include <sys/mman.h>

using namespace std;

namespace
{
    size_t getPageSize()
    {
        return (size_t) sysconf(_SC_PAGESIZE);
    }

    enum class CsvState
    {
        StartOfRow_e,
        StartOfCell_e,
        OnQuotedCell_e,
        OnEndOfQuotedCell_e,
        OnUnquotedCell_e,
        OnEndOfUnquotedCell_e,
        RowEnd_e,
    };

    const unsigned char utf8bom[] {0xEF, 0xBB, 0xBF};
    const unsigned char utf16be[] {0xFE, 0xFF};
    const unsigned char utf16le[] {0xFF, 0xFE};
    const unsigned char utf32be[] {0x00, 0x00, 0xFE, 0xFF};
    const unsigned char utf32le[] {0xFF, 0xFE, 0x00, 0x00};
    const unsigned char utf7[] {0x2B, 0x2F, 0x76};
    const unsigned char utf1[] {0xF7, 0x64, 0x4C};
    const unsigned char utfebcdic[] {0xDD, 0x73, 0x66, 0x73};
    const unsigned char scsu[] {0x0E, 0xFE, 0xFF};
    const unsigned char bocu1[] {0xFB, 0xEE, 0x28};
    const unsigned char gb18030[] {0x84, 0x31, 0x95, 0x33};
    
    const char* tryBomCheck(const char* start, const char* end)
    {
        size_t dataSz = static_cast<size_t>(end - start);
        if (dataSz >= sizeof(utf8bom) && memcmp(start, utf8bom, sizeof(utf8bom)) == 0)
        {   // match utf8 bom
            return start + sizeof(utf8bom);
        }

#define DETECT_AND_THROW(bomType) if (dataSz >= sizeof(bomType) && memcmp(start, bomType, sizeof(bomType)) == 0) \
        { \
            throw runtime_error("BOM indicates the CSV data is in unsupported encoding: " #bomType); \
        }

        DETECT_AND_THROW(utf16be);
        DETECT_AND_THROW(utf16le);
        DETECT_AND_THROW(utf32be);
        DETECT_AND_THROW(utf32le);
        DETECT_AND_THROW(utf7);
        DETECT_AND_THROW(utf1);
        DETECT_AND_THROW(utfebcdic);
        DETECT_AND_THROW(scsu);
        DETECT_AND_THROW(bocu1);
        DETECT_AND_THROW(gb18030);

#undef DETECT_AND_THROW

        return start;
    }
}


MmapCsvFileReader::MmapCsvFileReader(const string& filePath, char delimiter, char quote, bool skipEmptyRow)
: myRow(delimiter, quote), myDelimiter(delimiter), myQuote(quote), mySkipEmptyRow(skipEmptyRow),
  myQuotedCellSpanner(quote), myUnquotedCellSpanner(delimiter, '\r', '\n')
{
    // start mmap the file into memory
    filesystem::path f(filePath);
    size_t fileSize = filesystem::file_size(filePath);
    size_t pageSize = getPageSize();
    myMmapSize = static_cast<size_t>(std::ceil(static_cast<double>(fileSize + 16) / static_cast<double>(pageSize))) * pageSize;

    auto startp = (char *) mmap(0, myMmapSize, PROT_READ, MAP_ANON|MAP_PRIVATE, -1, 0);
    if(!startp)
    {
        throw runtime_error("mmap: failed to allocate required pages");
    }

    int fd = ::open(f.c_str(), O_RDONLY);
    if(fd == -1)
    {
        throw runtime_error("cannot open file "s + filePath + " for reading: " + strerror(errno));
    }

    pMyStart = static_cast<char*>(mmap(startp, fileSize, PROT_READ, MAP_PRIVATE | MAP_FIXED, fd, 0));
    ::close(fd);
    if(!pMyStart || startp != pMyStart)
    {
        throw runtime_error("mmap: failed to place file data before padded page");
    }

    ::madvise(pMyStart, fileSize, MADV_SEQUENTIAL);
    ::madvise(pMyStart, fileSize, MADV_WILLNEED);
    pMyEnd = pMyStart + fileSize;
    pMyCurrent = pMyStart;

    // does this data starts with a BOM?
    pMyCurrent = tryBomCheck(pMyCurrent, pMyEnd);
}

MmapCsvFileReader::~MmapCsvFileReader()
{
    if (pMyStart)
    {
        ::munmap(pMyStart, myMmapSize);
    }
}

bool MmapCsvFileReader::readRow()
{
    CsvState state = CsvState::StartOfRow_e;
    while(state != CsvState::RowEnd_e)
    {
        if (state == CsvState::StartOfRow_e)
        {
            // for the first row of entire data, we don't skip it even if it is empty
            // because this might be an empty header in most cases.
            myRow.markRowStart();
            if (pMyCurrent != pMyEnd)
            {
                state = CsvState::StartOfCell_e;
            }
            else
            {
                state = CsvState::RowEnd_e;
            }
        }
        
        if (state == CsvState::StartOfCell_e)
        {
            if (*pMyCurrent  == myQuote)
            {
                // it is a quoted cell, skip the starting quote
                ++pMyCurrent;
                state = CsvState::OnQuotedCell_e;
            }
            else
            {   // a unquoted cell, including empty unquoted cell
                state = CsvState::OnUnquotedCell_e;
            }
        }
        
        if (state == CsvState::OnQuotedCell_e)
        {
            // we are in a quoted cell state, and we have skipped the starting quote already
            myRow.markCellStart(pMyCurrent);
            while(pMyCurrent != pMyEnd)
            {
                // check next 16 bytes
                auto possibleMatchPos = myQuotedCellSpanner(pMyCurrent);
                pMyCurrent += possibleMatchPos;
                if (possibleMatchPos == 16)
                {
                    continue;
                }

                if (*pMyCurrent == myQuote)
                {
                    // we have a escape quote or ending quote?
                    if (pMyCurrent + 1 != pMyEnd && *(pMyCurrent + 1) == myQuote)
                    {
                        // we have a next char after this first quote in value
                        // and this is a two-double-quote escape for double-quote
                        // take it as content and put a double quite escape flag in the cell
                        myRow.markCellDoubleQuoteEscaped();
                        // skip to the char after escaped double quote
                        pMyCurrent += 2;
                    }
                    else
                    {
                        // encountered end of file, this is a ending quote
                        state = CsvState::OnEndOfQuotedCell_e;
                        break;
                    }
                }
                else
                {
                    // keep including data into the cell
                    ++pMyCurrent;
                }
            }
            if (pMyCurrent == pMyEnd)
            {
                // missing closing quote for the cell
                throw runtime_error("missing closing quote on cell");
            }
        }

        if (state == CsvState::OnEndOfQuotedCell_e)
        {
            // we are on the closing quote character now, set cell end and skip it
            myRow.markCellEnd(pMyCurrent);
            ++pMyCurrent;

            while (pMyCurrent != pMyEnd)
            {
                if (*pMyCurrent == myDelimiter)
                {
                    // starting a new cell
                    state = CsvState::StartOfCell_e;
                    ++pMyCurrent;
                    break;
                }
                else if (*pMyCurrent == '\r' || *pMyCurrent == '\n')
                {
                    state = CsvState::RowEnd_e;
                    break;
                }
                else
                {
                    // there is some garbage data after quoted cell end
                    // and before next cell or row end, we just throw them away
                    // like "asdf"   ,1234 or "asdf"  \r\n
                    ++pMyCurrent;
                }
            }

            if (pMyCurrent == pMyEnd)
            {   // this is like "asdf"  EOF
                state = CsvState::RowEnd_e;
            }
        }

        if (state == CsvState::OnUnquotedCell_e)
        {
            // start of unquoted cell state, and we know current position is at a unquoted cell start char
            myRow.markCellStart(pMyCurrent);
            // no matter wat happens, we need to handle the end of cell first, 
            // then potentially end of row and then potentially end of file
            state = CsvState::OnEndOfUnquotedCell_e;
            while(pMyCurrent != pMyEnd)
            {
                // check next 16 bytes
                auto possibleMatchPos = myUnquotedCellSpanner(pMyCurrent);
                pMyCurrent += possibleMatchPos;
                if (possibleMatchPos != 16)
                {
                    // encountered target character, new event is detected
                    break;
                }
            }
        }

        if (state == CsvState::OnEndOfUnquotedCell_e)
        {
            // we are on the delemiter/CR/LF/Eof characters following the end of unquoted cell data
            // we mark end of the current cell
            myRow.markCellEnd(pMyCurrent);
            if (*pMyCurrent == myDelimiter)
            {
                // we will have next cell, but not sure if it is quoted or not
                ++pMyCurrent;
                state = CsvState::StartOfCell_e;
            }
            else
            {
                // on end of the row, which means we are at last cell of the row
                state = CsvState::RowEnd_e;
            }
        }

        if (state == CsvState::RowEnd_e)
        {
            myRow.markRowEnd();
            if (mySkipEmptyRow)
            {
                while (pMyCurrent != pMyEnd && (*pMyCurrent == '\r' || *pMyCurrent == '\n'))
                {   // we are not at the end and we have CRLF character
                ++pMyCurrent;
                }
            }
            else
            {   // we need to consume at most one row delimiter if there is one
                vector<int> crlfCheck(4, 0);
                // recoganize different CRLF variants, as "\r\n", "\r[^\r\n]", "\n[^\r\n]" as a single row delimiter
                // and "\n\n", "\r\r" as two rows delimiters
                while (pMyCurrent != pMyEnd && (*pMyCurrent == '\r' || *pMyCurrent == '\n'))
                {
                    size_t index = static_cast<size_t>(*pMyCurrent - '\n');
                    if (crlfCheck[index]++ == 1)  // get the value for comparison first, then increase it by one
                    {   // we are at the second repeating row separator, we are at a new row already, don't skip
                        break;
                    }
                    else
                    {
                        ++pMyCurrent;
                    }
                }
            }
        }
    }
    return myRow.myValidCellCount > 0;
}

CsvRow::CsvRow(char delimiter, char quote)
: myDelimiter(delimiter), myQuote(quote)
{
    resize();
}

void CsvRow::resize()
{
    auto oldSize = myCells.size();
    auto size = oldSize * 2;
    if (size == 0)
    {
        size = 32;
    }
    myCells.resize(size);
    for(size_t i = oldSize; i < size; ++i)
    {
        CsvCell &cell = myCells[i];
        cell.myQuote = myQuote;
    }
}

CsvRow::const_iterator CsvRow::begin() const
{
    return myCells.cbegin();
}

CsvRow::const_iterator CsvRow::end() const
{
    auto it = myCells.cbegin();
    std::advance(it, myValidCellCount);
    return it;
}

void CsvRow::markCellStart(const char* startPos)
{
    if (myCells.size() < myValidCellCount + 1)
    {
        resize();
    }
    myCells[myValidCellCount].setStartPos(startPos);
}

void CsvRow::markCellEnd(const char* endPos)
{
    myCells[myValidCellCount].setEndPos(endPos);
    ++myValidCellCount;
}

void CsvRow::markCellDoubleQuoteEscaped()
{
    myCells[myValidCellCount].setDoubleQuoteEscaped();
}

void CsvRow::markRowStart()
{
    myValidCellCount = 0;
}

void CsvRow::markRowEnd()
{
    ++myRowCount;
}

string_view CsvRow::rawRowData() const
{
    if (myCells.empty())
    {
        return string_view();
    }
    else
    {
        auto start = myCells.front().pMyStart;
        const auto& lastCell = myCells.back();
        auto size = static_cast<size_t>(lastCell.pMyStart - start) + lastCell.mySize;
        return string_view(start, size);
    }
}

void CsvCell::setStartPos(const char* pos)
{
    pMyStart = pos;
    myHasEscapedQuote = false;
    myValue.reset();
}

void CsvCell::setEndPos(const char* pos)
{
    mySize = static_cast<size_t>(pos - pMyStart);
}

void CsvCell::setDoubleQuoteEscaped()
{
    myHasEscapedQuote = true;
}

string_view CsvCell::view() const
{
    while(true)
    {
        if (myValue)
        {
            return myValue->asView();
        }

        if (myHasEscapedQuote)
        {
            // we need to squizz the escape char out
            std::string result;
            result.reserve(mySize);
            for(size_t i = 0; i < mySize;)
            {
                if(pMyStart[i] == myQuote)
                {
                    ++i;
                }
                result.push_back(pMyStart[i++]);
            }
            myValue.emplace(std::move(result));
        }
        else
        {
            myValue.emplace(string_view(pMyStart, mySize));
        }
    }
}
