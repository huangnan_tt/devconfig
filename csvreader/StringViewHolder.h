#pragma once

#include <string_view>
#include <variant>
#include <string>

class StringViewHolder
{
public:

    template<typename T>
    StringViewHolder(T&& sv)
    : myValue(sv)
    {}

    std::string_view asView() const
    {
        return std::visit([](const auto& arg) -> std::string_view {return arg;}, myValue);
    }

private:
    std::variant<std::string_view, std::string> myValue;
};
