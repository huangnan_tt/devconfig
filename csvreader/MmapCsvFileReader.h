#pragma once

#include <cstring>
#include <string>
#include <string_view>
#include <vector>
#include <variant>
#include <optional>

class CsvCell;
class CsvRow;
class MmapCsvFileReader;


class StringVariant
{
public:

    template<typename T>
    StringVariant(T&& sv)
    : myValue(sv)
    {}

    std::string_view asView() const
    {
        return std::visit([](const auto& arg) -> std::string_view {return arg;}, myValue);
    }

private:
    std::variant<std::string_view, std::string> myValue;
};

class CsvCell
{
    friend class CsvRow;
public:
    std::string_view view() const;
private:
    void setStartPos(const char* pos);
    void setEndPos(const char* pos);
    void setDoubleQuoteEscaped();

    const char* pMyStart;
    size_t mySize;
    bool myHasEscapedQuote{};
    char myQuote;
    mutable std::optional<StringVariant> myValue;
};

class CsvRow
{
    friend MmapCsvFileReader;
public:
    using const_iterator = std::vector<CsvCell>::const_iterator;

    CsvRow(char delimiter, char quote);
    const_iterator begin() const;
    const_iterator end() const;
    std::string_view rawRowData() const;
protected:
    void markCellStart(const char* startPos);
    void markCellEnd(const char* endPos);
    void markCellDoubleQuoteEscaped();
    void markRowStart();
    void markRowEnd();

    void resize();
private:
    const char myDelimiter;
    const char myQuote;
    std::vector<CsvCell> myCells;
    size_t myValidCellCount{0};
    size_t myRowCount{0};
};



class MmapCsvFileReader
{
public:
    MmapCsvFileReader(const std::string& filePath, char delimiter = ',', char quote = '"', bool skipEmptyRow = false);

    ~MmapCsvFileReader();

    bool readRow();

    const CsvRow& rowData()
    {
        return myRow;
    }

private:
    struct StringSpannerSw
    {
        uint8_t charset_[256]{0};

        StringSpannerSw(char c1=0, char c2=0, char c3=0, char c4=0)
        {
            charset_[(unsigned) c1] = 1;
            charset_[(unsigned) c2] = 1;
            charset_[(unsigned) c3] = 1;
            charset_[(unsigned) c4] = 1;
            charset_[0] = 1;
        }

        size_t
        operator()(const char *s)
            __attribute__((__always_inline__))
        {
            auto p = (const unsigned char *)s;
            auto e = p + 16;

            do
            {
                if(charset_[p[0]]) {
                    break;
                }
                if(charset_[p[1]]) {
                    p++;
                    break;
                }
                if(charset_[p[2]]) {
                    p += 2;
                    break;
                }
                if(charset_[p[3]]) {
                    p += 3;
                    break;
                }
                p += 4;
            } while(p < e);

            return static_cast<size_t>(p - (const unsigned char *)s);
        }
    };

    char* pMyStart{};
    const char* pMyEnd{};
    const char* pMyCurrent{};
    size_t myMmapSize{};
    CsvRow myRow;
    const char myDelimiter;
    const char myQuote;
    const bool mySkipEmptyRow;
    StringSpannerSw myQuotedCellSpanner;
    StringSpannerSw myUnquotedCellSpanner;
};

