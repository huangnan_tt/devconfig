#pragma once

#include "StringViewHolder.h"

#include <cstring>
#include <iostream>
#include <vector>
#include <optional>
#include <memory>
#include <unistd.h>

namespace INFERNO
{
    class CsvStreamReader;
    class StreamIterator;
    struct StringSpannerSw;
    class CsvRow;

    class DataStream
    {
    public:
        ~DataStream() = default;
        virtual ssize_t getData(char*, ssize_t bufSz) = 0;
    };

    class FdStream : public DataStream
    {
    public:
        FdStream(int fd)
        : myFd(fd)
        {}

        ssize_t getData(char* buf, ssize_t bufSz) override
        {
            auto count = ::read(myFd, buf, static_cast<size_t>(bufSz));
            return count;
        }

        virtual ~FdStream()
        {
            if (myOwnFd)
            {
                ::close(myFd);
            }
        }

    private:
        int myFd{};
        bool myOwnFd{};
    };

    class StdIstream : public DataStream
    {
    public:
        StdIstream(std::istream& is)
        : myIs(is)
        {}

        StdIstream(std::unique_ptr<std::istream>&& is)
        : StdIstream(*is)
        {
            myUnderlying = std::move(is);
        }

        ssize_t getData(char* buf, ssize_t bufSz) override
        {
            myIs.read(buf, bufSz);
            auto count = myIs.gcount();
            if (count == 0 && myIs.eof())
            {
                return -1;
            }
            return count;
        }

    private:
        std::istream& myIs;
        std::unique_ptr<std::istream> myUnderlying;
    };

    class RowDataSource
    {
    public:
        RowDataSource(DataStream& stream);

        void startNewRow()
        {
            if (myRowEndOffset > 0)
            {
                size_t dataSize = myWritePos - myRowEndOffset;
                memmove(&myBuffer[0], &myBuffer[myRowEndOffset], dataSize);
                myWritePos = dataSize;
                myRowEndOffset = 0;
            }
        }

        void markRowEnd(const StreamIterator& endIt);

        /*
            Try to load the required position into row buffer 
            return true of the data at required offset is loaded, false otherwose.
        */
        bool ensureLoaded(size_t pos) const;

        char at(size_t offset)
        {
            ensureLoaded(offset);
            return myBuffer.at(offset);  // let it throw if overflow
        }

        const char* bufPosition(size_t offset) const
        {
            ensureLoaded(offset);
            return &myBuffer[offset];
        }

        StreamIterator begin();
        StreamIterator end();

    private:
        DataStream& myStream;
        mutable std::vector<char> myBuffer;
        mutable size_t myWritePos{};
        size_t myRowEndOffset{0};
    };

    class StreamIterator
    {
        friend struct StringSpannerSw;
        friend class RowDataSource;
        friend class CsvRow;
    public:
        StreamIterator(RowDataSource* source, size_t offset, bool intendedEnd)
        : mySource(source), myOffset(offset), myIntendedEnd(intendedEnd)
        {}

        StreamIterator& operator++()
        {
            // move the current cursor forward, and try to load more from underlying stream is not enough
            ++myOffset;
            return *this;
        }

        StreamIterator& operator+= (ssize_t n)
        {
            // move the current cursor forward, and try to load more from underlying stream is not enough
            if (n > 0)
            {
                myOffset += static_cast<size_t>(n);
            }
            else if (n < 0)
            {
                *this -= std::abs(n);
            }
            return *this;
        }

        StreamIterator& operator-= (ssize_t n)
        {
            // move the current cursor forward, and try to load more from underlying stream is not enough
            if (n > 0 && myOffset < static_cast<size_t>(n))
            {
                myOffset = 0;
            }
            else if (n < 0)
            {
                *this += std::abs(n);
            }
            return *this;
        }

        const char* bufPtr() const
        {
            sync();
            return mySource->bufPosition(myOffset);
        }

        char operator*()
        {
            // get the character from the current place
            return mySource->at(myOffset);
        }

        bool operator==(const StreamIterator& rhs);

        bool operator!=(const StreamIterator& rhs)
        {
            return !(*this == rhs);
        }

        StreamIterator operator+(ssize_t n) const
        {
            auto result = *this;
            if (n >= 0)
            {
                result.myOffset += static_cast<size_t>(n);
            }
            else
            {
                size_t back = static_cast<size_t>(std::abs(n));
                if (result.myOffset < back)
                {
                    result.myOffset = 0;
                }
                else
                {
                    result.myOffset -= back;
                }
            }

            return result;
        }

    private:

        void sync() const;

        RowDataSource* mySource;
        size_t myOffset;
        mutable bool myIntendedEnd;
    };

    class CsvRow
    {
        friend class CsvStreamReader;
    public:

        class CsvCell
        {
            friend class CsvRow;
        public:
            std::string_view view() const;
        private:
            void setStartPos(size_t offset)
            {
                myStartOffset = offset;
                myHasEscapedQuote = false;
                myValue.reset();
            }

            void setEndPos(size_t offset)
            {
                mySize = offset - myStartOffset;
            }

            void setDoubleQuoteEscaped()
            {
                myHasEscapedQuote = true;
            }

            size_t myStartOffset;
            size_t mySize;
            bool myHasEscapedQuote{};
            char myQuote;
            const RowDataSource* pMyRowDataSource;
            mutable std::optional<StringViewHolder> myValue;
        };

        using iterator = std::vector<CsvCell>::const_iterator;

        iterator begin() const;
        iterator end() const;

        size_t size() const
        {
            return myCells.size();
        }

        const CsvCell& operator[](size_t index) const
        {
            return myCells[index];
        }

        std::string_view rawRowData() const;

    protected:
        CsvRow(RowDataSource& source, char delimiter, char quote)
        : mySource(source), myDelimiter(delimiter), myQuote(quote)
        {
            resize();
        }

        void markCellStart(const StreamIterator& startIt)
        {
            if (myCells.size() < myValidCellCount + 1)
            {
                resize();
            }
            myCells[myValidCellCount].setStartPos(startIt.myOffset);
        }
        void markCellEnd(const StreamIterator& endIt)
        {
            myCells[myValidCellCount].setEndPos(endIt.myOffset);
            ++myValidCellCount;
        }
        void markCellDoubleQuoteEscaped()
        {
            myCells[myValidCellCount].setDoubleQuoteEscaped();
        }
        void markRowStart();
        void markRowEnd();

        void resize();
    private:
        RowDataSource& mySource;
        const char myDelimiter;
        const char myQuote;
        std::vector<CsvCell> myCells;
        size_t myValidCellCount{0};
        size_t myRowCount{0};
    };

struct StringSpannerSw
{
    uint8_t charset_[256]{0};

    StringSpannerSw(char c1=0, char c2=0, char c3=0, char c4=0)
    {
        charset_[(unsigned) c1] = 1;
        charset_[(unsigned) c2] = 1;
        charset_[(unsigned) c3] = 1;
        charset_[(unsigned) c4] = 1;
        charset_[0] = 1;
    }

    ssize_t
    operator()(const StreamIterator& it)
        __attribute__((__always_inline__))
    {
        auto eIt = it + 16;
        eIt.sync();   // load until the end, so we are sure the start required content are in the buffer
        auto p = (const unsigned char *)it.bufPtr();
        auto s = p;
        auto e = p + 16;
        do
        {
            if(charset_[p[0]]) {
                break;
            }
            if(charset_[p[1]]) {
                p++;
                break;
            }
            if(charset_[p[2]]) {
                p += 2;
                break;
            }
            if(charset_[p[3]]) {
                p += 3;
                break;
            }
            p += 4;
        } while(p < e);
        return p - s;
    }
};

class CsvStreamReader
{
public:
    CsvStreamReader(DataStream& stream, char delimiter = ',', char quote = '"', bool skipEmptyRow = true)
    : myInput(stream), myRow(myInput, delimiter, quote), myDelimiter(delimiter), myQuote(quote), mySkipEmptyRow(skipEmptyRow),
      myQuotedCellSpanner(quote), myUnquotedCellSpanner(delimiter, '\r', '\n')
    {}

    CsvStreamReader(std::unique_ptr<DataStream>&& stream, char delimiter = ',', char quote = '"', bool skipEmptyRow = true)
    : CsvStreamReader(*stream, delimiter, quote, skipEmptyRow)
    {
        myUnderlying = std::move(stream);
    }

    const CsvRow& rowData() const
    {
        return myRow;
    }

    bool readRow();

private:
    RowDataSource myInput;
    CsvRow myRow;
    const char myDelimiter;
    const char myQuote;
    const bool mySkipEmptyRow;
    StringSpannerSw myQuotedCellSpanner;
    StringSpannerSw myUnquotedCellSpanner;
    std::unique_ptr<DataStream> myUnderlying;
};
}