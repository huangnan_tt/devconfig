#include "CsvStreamReader.h"
#include <algorithm>
#include <vector>
#include <queue>
#include <cmath>
#include <iterator>

using namespace std;
using namespace INFERNO;

namespace
{
    enum class CsvState
    {
        StartOfRow_e,
        StartOfCell_e,
        OnQuotedCell_e,
        OnEndOfQuotedCell_e,
        OnUnquotedCell_e,
        OnEndOfUnquotedCell_e,
        RowEnd_e,
    };

    const unsigned char utf8bom[] {0xEF, 0xBB, 0xBF};
    const unsigned char utf16be[] {0xFE, 0xFF};
    const unsigned char utf16le[] {0xFF, 0xFE};
    const unsigned char utf32be[] {0x00, 0x00, 0xFE, 0xFF};
    const unsigned char utf32le[] {0xFF, 0xFE, 0x00, 0x00};
    const unsigned char utf7[] {0x2B, 0x2F, 0x76};
    const unsigned char utf1[] {0xF7, 0x64, 0x4C};
    const unsigned char utfebcdic[] {0xDD, 0x73, 0x66, 0x73};
    const unsigned char scsu[] {0x0E, 0xFE, 0xFF};
    const unsigned char bocu1[] {0xFB, 0xEE, 0x28};
    const unsigned char gb18030[] {0x84, 0x31, 0x95, 0x33};
    
    size_t tryBomCheck(const char* start, const char* end)
    {
        size_t dataSz = static_cast<size_t>(end - start);
        if (dataSz >= sizeof(utf8bom) && memcmp(start, utf8bom, sizeof(utf8bom)) == 0)
        {   // match utf8 bom
            return sizeof(utf8bom);
        }

#define DETECT_AND_THROW(bomType) if (dataSz >= sizeof(bomType) && memcmp(start, bomType, sizeof(bomType)) == 0) \
        { \
            throw runtime_error("BOM indicates the CSV data is in unsupported encoding: " #bomType); \
        }

        DETECT_AND_THROW(utf16be);
        DETECT_AND_THROW(utf16le);
        DETECT_AND_THROW(utf32be);
        DETECT_AND_THROW(utf32le);
        DETECT_AND_THROW(utf7);
        DETECT_AND_THROW(utf1);
        DETECT_AND_THROW(utfebcdic);
        DETECT_AND_THROW(scsu);
        DETECT_AND_THROW(bocu1);
        DETECT_AND_THROW(gb18030);

#undef DETECT_AND_THROW

        return 0;
    }
}


CsvRow::iterator CsvRow::begin() const
{
    return myCells.begin();
}

CsvRow::iterator CsvRow::end() const
{
    auto it = myCells.begin();
    std::advance(it, myValidCellCount);
    return it;
}

string_view CsvRow::rawRowData() const
{
    if (myCells.empty())
    {
        return string_view();
    }
    auto start = myCells.front().myStartOffset;
    const auto& lastCell = myCells.back();
    auto end = lastCell.myStartOffset + lastCell.mySize;
    return string_view(mySource.bufPosition(start), end - start);
}

void CsvRow::markRowStart()
{
    myValidCellCount = 0;
}

void CsvRow::markRowEnd()
{
    ++myRowCount;
}

void CsvRow::resize()
{
    auto oldSize = myCells.size();
    auto size = oldSize * 2;
    if (size == 0)
    {
        size = 32;
    }
    myCells.resize(size);
    for(size_t i = oldSize; i < size; ++i)
    {
        CsvCell &cell = myCells[i];
        cell.myQuote = myQuote;
        cell.pMyRowDataSource = &mySource;
    }
}

RowDataSource::RowDataSource(DataStream& stream)
: myStream(stream)
{
    // we need to load 4 bytes(offset 3) into the buffer for BOM testing
    ensureLoaded(3);
    const char* start = &myBuffer[0];
    size_t nShift = tryBomCheck(start, start+4);  // end is not included
    if (nShift)
    {
        auto endIt = myBuffer.begin();
        std::advance(endIt, nShift);
        myBuffer.erase(myBuffer.begin(), endIt);
    }
}

void RowDataSource::markRowEnd(const StreamIterator &endIt)
{
    myRowEndOffset = endIt.myOffset;
}

bool RowDataSource::ensureLoaded(size_t offset) const
{
    if (offset < myWritePos)
    {
        // no need to load
        return true;
    }

    // try to load some data
    size_t minToLoad = offset + 1 - myWritePos;
      // load 32 characters at least, zero-out the extra bytes if really no data
    auto sizeToLoad = std::max(1024l, static_cast<ssize_t>(minToLoad));
    auto newSize = myWritePos + static_cast<size_t>(sizeToLoad);
    if (myBuffer.size() < newSize)
    {
        myBuffer.resize(newSize);
    }
    auto count = myStream.getData(&myBuffer[myWritePos], sizeToLoad);
    if (count >= 0)
    {
        myWritePos += static_cast<size_t>(count);
    }
    // pad extra with zero
    bzero(&myBuffer[myWritePos], newSize - myWritePos);

    return offset < myWritePos;
}

StreamIterator RowDataSource::begin()
{
    return StreamIterator(this, 0, false);
}

StreamIterator RowDataSource::end()
{
    return StreamIterator(this, 0, true);
}

bool StreamIterator::operator==(const StreamIterator& other)
{
    if (mySource != other.mySource)
    {
        return false;
    }
    sync();
    other.sync();

    if (myIntendedEnd != other.myIntendedEnd)
    {   // one is end , and the other isn't
        return false;
    }

    if (myIntendedEnd)
    {   // both are at the end
        return true;
    }
    else
    {   // both are not at the end, then we need to check the offset
        return myOffset == other.myOffset;
    }
}

void StreamIterator::sync() const
{
    if (!myIntendedEnd)
    {
        // I need to be sure I have data at my current position
        myIntendedEnd = !mySource->ensureLoaded(myOffset);
    }
}

string_view CsvRow::CsvCell::view() const
{
    while(true)
    {
        if (myValue)
        {
            return myValue->asView();
        }

        if (myHasEscapedQuote)
        {
            // we need to squizz the escape char out
            std::string result;
            result.reserve(mySize);
            auto* pStart = pMyRowDataSource->bufPosition(myStartOffset);
            for(size_t i = 0; i < mySize;)
            {
                if(pStart[i] == myQuote)
                {
                    ++i;
                }
                result.push_back(pStart[i++]);
            }
            myValue.emplace(std::move(result));
        }
        else
        {
            myValue.emplace(string_view(pMyRowDataSource->bufPosition(myStartOffset), mySize));
        }
    }
}

bool CsvStreamReader::readRow()
{
    CsvState state = CsvState::StartOfRow_e;
    myInput.startNewRow();
    StreamIterator currentIt = myInput.begin();
    StreamIterator endIt = myInput.end();
    while(state != CsvState::RowEnd_e)
    {
        if (state == CsvState::StartOfRow_e)
        {
            // for the first row of entire data, we don't skip it even if it is empty
            // because this might be an empty header in most cases.
            myRow.markRowStart();
            if (currentIt != endIt)
            {
                state = CsvState::StartOfCell_e;
            }
            else
            {
                state = CsvState::RowEnd_e;
            }
        }
        
        if (state == CsvState::StartOfCell_e)
        {
            if (*currentIt == myQuote)
            {
                // it is a quoted cell, skip the starting quote
                ++currentIt;
                state = CsvState::OnQuotedCell_e;
            }
            else
            {   // a unquoted cell, including empty unquoted cell
                state = CsvState::OnUnquotedCell_e;
            }
        }
        
        if (state == CsvState::OnQuotedCell_e)
        {
            // we are in a quoted cell state, and we have skipped the starting quote already
            myRow.markCellStart(currentIt);
            while (currentIt != endIt)
            {
                // check next 16 bytes
                auto possibleMatchPos = myQuotedCellSpanner(currentIt);
                currentIt += possibleMatchPos;
                if (possibleMatchPos == 16)
                {
                    continue;
                }

                if (*currentIt == myQuote)
                {
                    // we have a escape quote or ending quote?
                    if (currentIt + 1 != endIt && *(currentIt + 1) == myQuote)
                    {
                        // we have a next char after this first quote in value
                        // and this is a two-double-quote escape for double-quote
                        // take it as content and put a double quite escape flag in the cell
                        myRow.markCellDoubleQuoteEscaped();
                        // skip to the char after escaped double quote
                        currentIt += 2;
                    }
                    else
                    {
                        // encountered end of file, this is a ending quote
                        state = CsvState::OnEndOfQuotedCell_e;
                        break;
                    }
                }
                else
                {
                    // keep including data into the cell
                    ++currentIt;
                }
            }
            if (currentIt == endIt)
            {
                // missing closing quote for the cell
                throw runtime_error("missing closing quote on cell");
            }
        }

        if (state == CsvState::OnEndOfQuotedCell_e)
        {
            // we are on the closing quote character now, set cell end and skip it
            myRow.markCellEnd(currentIt);
            ++currentIt;

            while (currentIt != endIt)
            {
                if (*currentIt == myDelimiter)
                {
                    // starting a new cell
                    state = CsvState::StartOfCell_e;
                    ++currentIt;
                    break;
                }
                else if (*currentIt == '\r' || *currentIt == '\n')
                {
                    state = CsvState::RowEnd_e;
                    break;
                }
                else
                {
                    // there is some garbage data after quoted cell end
                    // and before next cell or row end, we just throw them away
                    // like "asdf"   ,1234 or "asdf"  \r\n
                    ++currentIt;
                }
            }

            if (currentIt == endIt)
            {   // this is like "asdf"  EOF
                state = CsvState::RowEnd_e;
            }
        }

        if (state == CsvState::OnUnquotedCell_e)
        {
            // start of unquoted cell state, and we know current position is at a unquoted cell start char
            myRow.markCellStart(currentIt);
            // no matter wat happens, we need to handle the end of cell first, 
            // then potentially end of row and then potentially end of file
            state = CsvState::OnEndOfUnquotedCell_e;
            while (currentIt != endIt)
            {
                // check next 16 bytes
                auto possibleMatchPos = myUnquotedCellSpanner(currentIt);
                currentIt += possibleMatchPos;
                if (possibleMatchPos != 16)
                {
                    // encountered target character, new event is detected
                    break;
                }
            }
        }

        if (state == CsvState::OnEndOfUnquotedCell_e)
        {
            // we are on the delemiter/CR/LF/Eof characters following the end of unquoted cell data
            // we mark end of the current cell
            myRow.markCellEnd(currentIt);
            if (*currentIt == myDelimiter)
            {
                // we will have next cell, but not sure if it is quoted or not
                ++currentIt;
                state = CsvState::StartOfCell_e;
            }
            else
            {
                // on end of the row, which means we are at last cell of the row
                state = CsvState::RowEnd_e;
            }
        }

        if (state == CsvState::RowEnd_e)
        {
            myRow.markRowEnd();
            if (mySkipEmptyRow)
            {

                while (currentIt != endIt && (*currentIt == '\r' || *currentIt == '\n'))
                {   // we are not at the end and we have CRLF character
                    ++currentIt;
                }
                myInput.markRowEnd(currentIt);
            }
            else
            {   // we need to consume at most one row delimiter if there is one
                // recoganize different CRLF variants, as "\r\n", "\r[^\r\n]", "\n[^\r\n]" as a single row delimiter
                // and "\n\n", "\r\r" as two rows delimiterss
                vector<int> crlfCheck(4, 0);
                while (currentIt != endIt && (*currentIt == '\r' || *currentIt == '\n'))
                {   // we are not at the end and we have CRLF character
                    size_t index = static_cast<size_t>(*currentIt - '\n');
                    if (crlfCheck[index]++ == 1)  // get the value for comparison first, then increase it by one
                    {   // we are at the second repeating row separator, we are at a new row already, don't skip
                        myInput.markRowEnd(currentIt);
                        break;
                    }
                    else
                    {
                        ++currentIt;
                    }
                }
            }
        }
    }
    return myRow.myValidCellCount > 0;
}